@extends('layouts.app')

@section('content')

@if($errors->any())
    <div role="alert w-1/4">
    <div class="bg-red-500 w-1/4 text-white font-bold rounded-t px-4 py-2">
        There were problem with your input.
    </div>
    <div class="w-1/4 border border-t-0 border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    </div>
@endif
        <div class="w-11/12 md:w-2/3 lg:w-2/3 xl:w-2/3 md:mx-auto lg:mx-auto xl:mx-auto mx-2">

            <div class="container flex justify-end">
                @if(Auth::user())
                <div class="flex my-4">
                    <a href="{{ route('user.categories.create') }}" class="btn btn-blue">New Category</a>
                </div>
                @endif
            </div>

            <div class="bg-white shadow-md rounded my-6 mx-auto shadow-xl">
            <table class="text-left w-full">
            <thead>
                <tr>
                    <th class="py-4 px-6 font-bold uppercase text-sm text-gray-dark">Name</th>
                    <th class="py-4 px-6 font-bold uppercase text-sm text-gray-dark"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($categories as $category)
                    <tr class="hover:bg-green-100">
                        @if(Auth::user())
                            <td class="py-4 px-2 md:px-6"><a class="text-xs md:text-lg btn bg-indigo-500 hover:bg-indigo-700" href="{{ route('user.categories.show', $category) }}">{{ $category->name }}</a></td>
                                @if($category->categoryOwner())
                                    <td class="py-4 px-2 md:px-6"><a href="{{ route('user.categories.edit', $category) }}" class="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded">
                                            Edit
                                        </a>
                                    </td>

                                    <td class="py-4 px-2 md:px-6">
                                        <form class="inline sm:inline-block" action="{{ route('user.categories.destroy', $category) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                            <button
                                                class="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 rounded" type="submit">
                                                Delete
                                            </button>
                                        </form>      
                                    </td>
                                @endif
                        @else
                            <td class="py-4 px-2 md:px-6"><a class="text-xs md:text-lg btn bg-indigo-500 hover:bg-indigo-700" href="{{ route('categories.show', $category) }}">{{ $category->name }}</a></td>
                        @endif
                    </tr>
                @endforeach
            </tbody>
        </table>
        </div>
    </div>
    
	
        {{ $categories->links() }}
@endsection
