@extends('layouts.app')

@section('content')
@if($errors->any())
    <div role="alert w-1/4">
    <div class="bg-red-500 w-1/4 text-white font-bold rounded-t px-4 py-2">
        There were problem with your input.
    </div>
    <div class="w-1/4 border border-t-0 border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    </div>
@endif
    <div class="flex justify-center">
        <div class="container p-2 lg:p-16 bg-white lg:w-2/3 w-11/12 rounded-lg shadow-xl">
            <div class="flex mb-8 bg-white">
                <h1 class="text-black text-xl font-bold uppercase mx-auto">Edit Category</h1>
            </div>
            <form action="{{ route('user.categories.update', $category) }}" method="POST">
            @csrf
            @method('PATCH')
                <div class="container my-4">
                        <strong class="p-2">Category:</strong>
                        <input type="text" name="name" class="w-full h-10 p-2 border" placeholder="category-name" value="{{ $category->name }}">
                </div>
                <div class="flex justify-between mt-8">
                    <a href="{{ route('user.categories.index') }}" class="btn btn-blue text-xs md:text-lg">Cancel</a>
                    <button type="submit" class="btn btn-blue text-xs md:text-lg">Edit</button>
                </div>
            </form>
        </div>
    </div>
@endsection