@extends('layouts.app')

@section('head')
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5e1eeef9cf446508"></script>
<script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
<script>tinymce.init({ selector:'textarea' });</script>

@endsection

@section('content')
<div class="flex justify-center">
<div class="container p-2 lg:p-16 bg-white lg:w-2/3 w-11/12 rounded-lg shadow-xl">
    <div class="flex mb-8">
        <h1 class="text-black text-xl font-bold uppercase mx-auto">Edit Comment</h1>
    </div>
    
    <form action="{{ route('user.comments.update', $comment) }}" method="POST">
        @csrf
        @method('PATCH')
        <div class="container my-4">
            <textarea name="title" class="shadow appearance-none border mt-2 rounded mb-4 w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" type="text">{{ clean($comment->title) }}
            </textarea>
        </div>

        <input type="hidden" name="post_id" value="{{ $post->id }}">

        <div class="container my-4 flex justify-between">
            <a href="{{ route('user.posts.show', $comment->post) }}" class="btn btn-blue text-xs md:text-lg">Cancel</a>
            <button type="submit" class="btn btn-blue text-xs md:text-lg">Edit</button>
        </div>
    </form>
    
</div>
</div>
@endsection
