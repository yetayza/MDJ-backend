@extends('layouts.app')

@section('content')

<div class="bg-gray-300 flex flex-col">
    <div class="container max-w-sm mx-auto flex-1 flex flex-col items-center justify-center px-2">
        <div class="bg-white px-6 py-8 rounded shadow-md text-black w-full">
            <h1 class="mb-8 text-3xl text-center">{{ __('Reset Password') }}</h1>
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <form method="POST" action="{{ route('password.update') }}">
                @csrf
                <input type="hidden" name="token" value="{{ $token }}">
                <label class="uppercase tracking-wide text-black text-sm font-bold mb-2" for="email">
                    {{ __('E-Mail Address') }}
                </label>
                <input id="email" placeholder="{{ __('E-Mail Address') }}" type="email" class="shadow appearance-none mt-2 border rounded mb-4 w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                <div>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <label class="uppercase tracking-wide text-black text-sm font-bold mb-2" for="password">
                    {{ __('Password') }}
                </label>
                <input id="password" placeholder="{{ __('Password') }}" type="password" class="shadow appearance-none mt-2 border rounded w-full mb-4 py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                
                <label class="uppercase tracking-wide text-black text-sm font-bold mb-2" for="password">
                    {{ __('Confirm Password') }}
                </label>
                <input id="password-confirm" placeholder="{{ __('Confirm Password') }}" type="password" class="shadow appearance-none mt-2 border rounded w-full mb-4 py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="password_confirmation" required autocomplete="new-password">
                <div class="flex mt-4">
                    <button class="w-full text-center py-3 md:w-sm bg-blue-800 mx-auto text-white font-bold px-4 border-b-4 hover:border-b-2 border-gray-500 hover:border-gray-100 rounded" type="submit">
                    {{ __('Reset Password') }}
                    </button>
                </div>
            </form>
        </div>  
    </div>
</div>

@endsection
