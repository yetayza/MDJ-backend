@extends('layouts.app')

@section('content')
    <div class="bg-gray-300 min-h-screen flex flex-col">
        <form method="POST" action="{{ route('register') }}">
            @csrf
                <div class="container max-w-sm mx-auto flex-1 flex flex-col items-center justify-center px-2">
                    <div class="bg-white px-6 py-8 rounded shadow-md text-black w-full">
                        <h1 class="mb-8 text-3xl text-center">Sign up</h1>
                            <label class="uppercase tracking-wide text-black text-sm font-bold mb-2" for="name">
                                {{ __('Name') }}
                            </label>
                            <input id="name" placeholder="{{ __('Name') }}" type="text" class="shadow appearance-none border mt-2 rounded mb-4 w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            @error('name')
                                <span class="text-red-500 text-xs italic" role="alert">
                                            <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                            <label class="uppercase tracking-wide text-black text-sm font-bold mb-2" for="email">
                                {{ __('E-Mail Address') }}
                            </label>
                            <input id="email" placeholder="{{ __('E-Mail Address') }}" type="email" class="shadow appearance-none mt-2 border rounded mb-4 w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                            <div>
                                @error('email')
                                    <span class="text-red-500 text-xs italic" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <label class="uppercase tracking-wide text-black text-sm font-bold mb-2" for="password">
                                {{ __('Password') }}
                            </label>
                            <input id="password" placeholder="{{ __('Password') }}" type="password" class="shadow appearance-none mt-2 border rounded w-full mb-4 py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                            @error('password')
                                <span class="text-red-500 text-xs italic" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            
                            <label class="uppercase tracking-wide text-black text-sm font-bold mb-2" for="password">
                                {{ __('Confirm Password') }}
                            </label>
                            <input id="password-confirm" placeholder="{{ __('Confirm Password') }}" type="password" class="shadow appearance-none mt-2 border rounded w-full mb-4 py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="password_confirmation" required autocomplete="new-password">
                    
                            <div class="flex mt-4">
                                <button class="w-full text-center py-3 md:w-sm bg-blue-800 mx-auto text-white font-bold px-4 border-b-4 hover:border-b-2 border-gray-500 hover:border-gray-100 rounded" type="submit">
                                {{ __('Create Account') }}
                                </button>
                            </div>

                            <div class="text-center text-sm text-grey-dark mt-4">
                                Already have an account? 
                                <a class="text-blue" href="{{ route('login') }}">
                                    Log in
                                </a>.
                            </div>
                        </div>    
                </div>
        </form>
    </div>
@endsection
