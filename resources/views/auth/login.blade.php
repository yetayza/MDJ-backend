@extends('layouts.app')

@section('content')
        <div class="w-full max-w-xs contaier-md h-screen mx-auto">
            <form class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4 mx-auto" method="POST" action="{{ route('login') }}">
                @csrf

                <div class="mb-4 mx-auto">
                    <label class="block text-gray-700 text-sm font-bold mb-2" for="email">
                        {{ __('E-Mail Address') }}
                    </label>
                    <input placeholder="{{ __('E-Mail Address') }}" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline @error('email') is-invalid @enderror" id="email" name="email" type="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                            <span class="text-red-500 text-xs italic" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                         @enderror
                </div>
                <div class="mb-6 mx-auto">
                    <label class="block text-gray-700 text-sm font-bold mb-2" for="password">
                        {{ __('Password') }}
                    </label>
                    <input placeholder="{{ __('Password') }}" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline @error('password') is-invalid @enderror" name="password" id="password" type="password" required autocomplete="current-password">
                        @error('password')
                            <span class="text-red-500 text-xs italic" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>
                <div class="mb-4 mx-auto">
                    <label class="checkbox" for="remember">
                        <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                    </label>
                </div>
                <div class="flex items-center justify-between">
                    <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                        {{ __('Login') }}
                    </button>
                    @if (Route::has('password.request'))
                        <a class="inline-block align-baseline font-bold text-sm text-blue-500 hover:text-blue-800" href="{{ route('password.request') }}">
                            {{ __('Forgot Your Password?') }}
                        </a>
                    @endif
                </div>
            </form>
                <p class="text-center text-gray-500 text-sm">
                    Don't have an account? 
                    <a href="{{ route('register') }}" class="no-underline text-blue font-bold">
                        Create an Account
                    </a>.
                </p>
        </div>
@endsection
