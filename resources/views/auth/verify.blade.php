@extends('layouts.app')

@section('content')
<div class="bg-gray-300 flex flex-col">
    <div class="container max-w-sm mx-auto flex-1 flex flex-col items-center justify-center px-2">
        <div class="bg-white px-6 py-8 rounded shadow-md text-black w-full">
            <label class="uppercase tracking-wide text-black text-sm font-bold mb-2">
               {{ __('Verify Your Email Address') }}
            </label>
                
                    @if (session('resent'))
                        <div role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                       
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }},
                    <form class="" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="w-full text-center py-3 md:w-sm bg-blue-800 mx-auto text-white font-bold px-4 border-b-4 hover:border-b-2 border-gray-500 hover:border-gray-100 rounded">{{ __('click here to request another email verification link') }}</button>.
                    </form>
                </div>
            
        </div>
    </div>
</div>
@endsection
