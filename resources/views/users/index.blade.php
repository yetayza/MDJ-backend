@extends('layouts.app')

@section('content')
	<div class="container max-w-sm mx-auto flex-1 flex flex-col items-center justify-center px-2">
        <div class="bg-white px-6 py-8 rounded shadow-md text-black w-full">
            <div class="flex">
                <a class="mx-auto" href="{{ route('profile.show', $user) }}">
                    <img class="rounded-full" src="{{ asset('/'. $user->avatar  ) }}" style="height: 50px; width: 50px;"/>
                </a>
            </div>
            <div class="flex mt-3">
                <a class="mx-auto" href="{{ route('profile.show', $user) }}">
                    <span class="font-bold text-xl mx-auto">{{$user->name}}</span>
                </a>
            </div>
            <div class="flex mt-4">
                <a href="{{ route('user.change.picture.edit', $user) }}" class="w-full text-center py-3 md:w-sm bg-blue-800 mx-auto text-white font-bold px-4 border-b-4 hover:border-b-2 border-gray-500 hover:border-gray-100 rounded" type="submit">
                    Change Profile Picture
                </a>
            </div>
            <div class="flex mt-4">
                <a href="{{ route('user.change.password.edit', $user) }}" class="w-full text-center py-3 md:w-sm bg-blue-800 mx-auto text-white font-bold px-4 border-b-4 hover:border-b-2 border-gray-500 hover:border-gray-100 rounded" type="submit">
                Change Password
                </a>
            </div>
            <div class="flex mt-4">
                <a href="{{ route('user.change.username.edit', $user) }}" class="w-full text-center py-3 md:w-sm bg-blue-800 mx-auto text-white font-bold px-4 border-b-4 hover:border-b-2 border-gray-500 hover:border-gray-100 rounded" type="submit">
                Change Username
                </a>
            </div>
        </div>
    </div>
@endsection
