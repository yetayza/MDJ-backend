@extends('layouts.app')

@section('head')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>
@endsection

@section('content')

@if($errors->any())
    <div class="alert alert-danger">
        
            <button type="button" class="close" data-dismiss="alert">×</button> 
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="bg-gray-300 flex flex-col">
    <div class="container max-w-sm mx-auto flex-1 flex flex-col items-center justify-center px-2">
        <div class="bg-white px-6 py-8 rounded shadow-md text-black w-full">  
            <form action="{{ route('user.change.username.update', $user) }}" method="POST">
                @csrf
                @method ('PATCH')
                <label class="uppercase tracking-wide text-black text-sm font-bold mb-2" for="name">
                {{ __('User Name') }}
                </label>
                <input id="name" value="{{ $user->name ?? old('name') }}" type="text" class="shadow appearance-none mt-2 border rounded w-full mb-4 py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="name" required autocomplete="name">
                
                <div class="flex my-4">
                    <button type="submit" class="w-full text-center py-3 md:w-sm bg-blue-800 mx-auto text-white font-bold px-4 border-b-4 hover:border-b-2 border-gray-500 hover:border-gray-100 rounded">Save Changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection