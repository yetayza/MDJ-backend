@extends('layouts.app')

@section('content')

<div class="bg-gray-300 flex flex-col">
    <div class="container max-w-sm mx-auto flex-1 flex flex-col items-center justify-center px-2">
        <div class="bg-white px-6 py-8 rounded shadow-md text-black w-full">
            <h1 class="mb-8 text-3xl text-center">{{ __('Change Password') }}</h1>
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <form method="POST" action="{{ route('user.change.password.update', $user) }}">
                @csrf
                @foreach ($errors->all() as $error)
                <div class="bg-red-300 border rounded shadow-lg">
                    <button type="button" class="close" data-dismiss="alert">×</button>	
                        <strong class="text-red-600">{{ $error }}</strong>
                </div>
                @endforeach 

                <label class="uppercase tracking-wide text-black text-sm font-bold mb-2" for="email">
                    {{ __('Current Password') }}
                </label>
                <input id="password" placeholder="Current Password" type="password" class="shadow appearance-none mt-2 border rounded mb-4 w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline @error('password') is-invalid @enderror" name="current_password" required autocomplete="current_password" autofocus>
                <div>
                    @error('password')
                        <span class="text-red-600" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <label class="uppercase tracking-wide text-black text-sm font-bold mb-2" for="password">
                    {{ __('New Password') }}
                </label>
                <input id="new_password" placeholder="{{ __('New Password') }}" type="password" class="shadow appearance-none mt-2 border rounded w-full mb-4 py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline @error('password') is-invalid @enderror" name="new_password" required autocomplete="new-password">
                @error('password')
                    <span class="text-red-600" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                
                <label class="uppercase tracking-wide text-black text-sm font-bold mb-2" for="password">
                    {{ __('Confirm New Password') }}
                </label>
                <input id="new_confirm_password" placeholder="{{ __('Confirm Password') }}" type="password" class="shadow appearance-none mt-2 border rounded w-full mb-4 py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline @error('password') is do not match @enderror" name="new_confirm_password" required autocomplete="new-password">
                @error('password')
                    <span class="text-red-600" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <div class="flex mt-4">
                    <button class="w-full text-center py-3 md:w-sm bg-blue-800 mx-auto text-white font-bold px-4 border-b-4 hover:border-b-2 border-gray-500 hover:border-gray-100 rounded" type="submit">
                    {{ __('Change Password') }}
                    </button>
                </div>
            </form>
        </div>  
    </div>
</div>

@endsection
