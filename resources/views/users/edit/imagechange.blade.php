@extends('layouts.app')

@section('head')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>
@endsection

@section('content')

@if($errors->any())
    <div class="alert alert-danger">
        
            <button type="button" class="close" data-dismiss="alert">×</button> 
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

        <div class="bg-white px-6 py-8 rounded shadow-md text-black w-full">
            <form action="{{ route('user.change.picture.update', $user) }}" method="POST" enctype="multipart/form-data" class="w-full bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4 mx-auto">
                @csrf
                <div class="flex my-4 p-2 w-full">
                    <strong class="p-2">Image</strong>
                    <input type="file" name="avatar" class="p-2 w-full border">
                </div>
                
                <div class="flex my-4">
                    <button type="submit" class="btn btn-blue mx-auto">Submit</button>
                </div>
            </form>
        </div>
 

@endsection