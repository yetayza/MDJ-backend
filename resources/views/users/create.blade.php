@extends('layouts.app')

@section('head')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>
@endsection

@section('content')

@if($errors->any())
    <div class="alert alert-danger">
        <strong>Opps!</strong> There were some problems with your input.<br><br>
            <button type="button" class="close" data-dismiss="alert">×</button>	
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('users.update', $user) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>User Name</strong>
                        <input type="text" name="title" class="form-control" value="{{ $user->name }}">
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Image</strong>
                        <input type="file" name="avatar" class="form-control">
                    </div>
                </div>

                
   
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
   
            </div>
</form>
@endsection