@extends('layouts.app')

@section('content')
	
	<div class="w-11/12 md:w-2/3 lg:w-2/3 xl:w-2/3 md:mx-auto lg:mx-auto xl:mx-auto mx-2">
        <div class="bg-white shadow-md rounded my-6 mx-auto shadow-xl">
            <table class="text-left w-full">
	            <thead>
	                <tr>
	                    <th class="py-4 px-6 font-bold uppercase text-sm text-gray-dark">Name</th>
	                </tr>
	            </thead>
	            <tbody>
	                @foreach($categories as $category)
	                    <tr class="hover:bg-green-100">
	                            <td class="py-4 px-2 md:px-6">
	                            	<a class="text-xs md:text-lg btn bg-indigo-500 hover:bg-indigo-700" href="{{ route('categories.show', $category) }}">{{ $category->name }}
	                            	</a>
	                            </td>
	                    </tr>
	                @endforeach
	            </tbody>
	        </table>
    	</div>
    </div>
    
        {{ $categories->links() }}

@endsection