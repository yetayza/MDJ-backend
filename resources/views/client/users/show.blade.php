@extends('layouts.app')


@section('content')

<div class="container mx-auto">
	<div class="rounded mx-auto bg-white rounded-t-lg overflow-hidden shadow max-w-xs my-3 ">
		<img src="https://i.imgur.com/dYcYQ7E.png" class="w-full"/>
		<div class="flex justify-center -mt-8">
		<a href="{{ route('profile.show', $user) }}">
			<img src="{{ asset('/'. $user->avatar  ) }}" style="height: 50px; width: 50px;" class="rounded-full border-solid border-white border-2 -mt-3">
		</a>		
		</div>
		<div class="text-center px-3 pb-6 pt-2">
			<h3 class="text-black text-sm bold font-sans">{{ $user->name }}</h3>
			<p class="mt-2 font-sans font-light text-grey-dark">Hello, i'm depoloper!</p>
		</div>
		<div class="flex justify-center pb-3 text-grey-dark">
		<div class="text-center mr-3 pr-3">
			<h2>{{ $posts->count() }}</h2>
			<span>Posts</span>
		</div>
		</div>
	</div>

	<div class="flex p-2">
        <h1 class="text-xl mx-auto">All Posts By <strong>{{ $user->name }}</strong></h1>
    </div>
</div>
<!-- cards -->
<div class="container my-2 mx-auto px-4 md:px-12">
    <div class="flex flex-wrap -mx-1 lg:-mx-4">
        @foreach($posts as $post)
  
		<div class="my-4 px-1 w-full md:w-1/2 lg:my-4 lg:px-4 lg:w-1/3">
                <div class="w-full max-w-sm overflow-hidden rounded border bg-white hover:shadow-xl mx-auto">
                    <div class="relative">
                        <a href="{{ route('posts.show', $post) }}">    
                            <img class="w-full object-fill h-40" src="{{ ('/'. $post->cover) }}">
                        </a>
                    <div style="bottom: -20px;" class="absolute right-0 w-10 mr-2">
                        <a href="{{ route('profile.show', $post->user) }}">
                        <img class="rounded-full border-2 border-white" style="height: 40px; width: 40px;" src="{{ asset('/'. $post->user->avatar) }}" >
                        </a>
                    </div>
                    </div>
                    <div class="p-3">
                    <h3 class="mr-10 text-sm truncate-2nd">
                        <a class="hover:text-blue-500" href="{{ route('posts.show', $post) }}">{{ Str::limit($post->title, 30) }}</a>
                    </h3>
                    <div class="flex items-start justify-between">
                        <p class="text-xs text-gray-500">{{ Str::limit($post->short_des, 35) }}</p>
                    </div>
                    <p class="text-xs text-gray-500"><a href="{{ route('profile.show', $post->user) }}" class="hover:underline hover:text-blue-500">{{ $post->user->name }}</a> • {{ $post->created_at->diffForHumans() }}</p>
                    </div>
                </div>
            </div>
        
        @endforeach
    </div>
</div>
	{{ $posts->links() }}
@endsection