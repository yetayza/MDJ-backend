@extends('layouts.app')

@section('head')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>
@endsection

@section('content')

@if($errors->any())
    <div class="alert alert-danger">
        <strong>Opps!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="flex justify-center">
<div class="container p-2 lg:p-16 bg-white lg:w-2/3 w-11/12 rounded-lg">
    <div class="flex mb-8">
        <h1 class="text-black text-3xl font-bold uppercase mx-auto">Edit Post</h1>
    </div>
    <form action="{{ route('user.posts.update', $post) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
        <div class="container my-4">
            <strong>Title</strong>
            <input type="text" name="title" class="shadow appearance-none border mt-2 rounded mb-4 w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" placeholder="Title" value="{{ $post->title }}">
        </div>
        

        <div class="container my-4">
            <strong>Description</strong>
            <input type="text" name="short_des" class="shadow appearance-none border mt-2 rounded mb-4 w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" value="{{ $post->short_des }}">
        </div>

        
        <div class="container my-4">
            <strong>Full Article</strong>
            <textarea name="body" class="shadow appearance-none border mt-2 rounded mb-4 w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" type="text">{{ clean($post->body) }}</textarea>
        </div>

        <div class="container my-4">
            <strong class="block mb-2">Image:</strong>
            <label class="block text-sm text-gray-700 mb-2">• Acceptable image dimensions are minimum 900x300 and maximum 1250x500.</label>
            <label class="block text-sm text-gray-700">• File size cannot exceed 2 MB.</label> 
            
            <input type="file" name="cover" class="shadow appearance-none border mt-2 rounded mb-4 w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
        </div>
       

        <div class="container my-4">
            <label>Category</label>
            <select class="shadow appearance-none border mt-2 rounded mb-4 w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline bg-white" name="category_id">
                @foreach($categories as $category)
                    <option value="{{$category->id}}">
                        {{$category->name}}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="container my-4 flex justify-between">
            <a href="{{ route('user.posts.index') }}" class="btn btn-blue text-xs md:text-lg">Cancel</a>
            <button type="submit" class="btn btn-blue text-xs md:text-lg">Submit</button>
        </div>
    </form>
</div>
</div>
@endsection