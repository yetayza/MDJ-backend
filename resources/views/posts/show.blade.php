@extends('layouts.app')

@section('head')
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5e1eeef9cf446508"></script>
<script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
<script>tinymce.init({ selector:'textarea' });</script>

@endsection

@section('content')
    @if($errors->any())
    <div class="flex items-center bg-blue-500 w-1/4 text-white text-sm font-bold px-4 py-3" role="alert">
    <svg class="fill-current w-4 h-4 mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M12.432 0c1.34 0 2.01.912 2.01 1.957 0 1.305-1.164 2.512-2.679 2.512-1.269 0-2.009-.75-1.974-1.99C9.789 1.436 10.67 0 12.432 0zM8.309 20c-1.058 0-1.833-.652-1.093-3.524l1.214-5.092c.211-.814.246-1.141 0-1.141-.317 0-1.689.562-2.502 1.117l-.528-.88c2.572-2.186 5.531-3.467 6.801-3.467 1.057 0 1.233 1.273.705 3.23l-1.391 5.352c-.246.945-.141 1.271.106 1.271.317 0 1.357-.392 2.379-1.207l.6.814C12.098 19.02 9.365 20 8.309 20z"/></svg>
            <p><ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul></p>
    </div>
    @endif
    <div class="bg-gray-300 p-2 md:p-4">
        <div class="lg:w-2/3 sm:w-11/12 mx-auto p-4 shadow rounded-lg bg-white">
            <div class="my-3">
                <h2 class="text-xl font-bold uppercase">{{ $post->title }}</h2>
            </div>

            <div class="my-3">
                <a class="mx-auto" href="{{ route('user.posts.show', $post) }}">
                    <img src="{{ asset('/'. $post->cover) }}" alt="" class="object-fill h-auto md:object-cover w-full">
                </a>
            </div>

            <div class="my-3">
                <p class="text-sm lg:text-xl">
                    {!! clean($post->body) !!}
                </p>
            </div>

            <div class="my-8">
                <a href="{{ route('user.profile.show', $post->user) }}">
                    <strong>
                        By {{ $post->user->name }}
                    </strong>
                </a>
                        at {{ $post->created_at->diffForHumans() }}.
            </div>

            <div class="my-3">
                <a href="{{ route('user.profile.show', [$post->user]) }}">
                <img class="rounded-full" style="height: 50px; width: 50px;"  src="{{ asset('/'. $post->user->avatar  ) }}" />
                </a>
            </div>

            <div class="my-4">
                <label>Category:</label>
                @if(Auth::user())
                <a href="{{ route('user.categories.show', $post->category->name) }}" class="btn btn-blue">{{ $post->category->name }}</a>
                @else
                <a href="{{ route('categories.show', $post->category->name) }}" class="btn btn-blue">{{ $post->category->name }}</a>
                @endif
            </div>

            <div class="addthis_inline_share_toolbox my-3">
                
            </div>

            @if(Auth::user())
            <div class="my-8">
                <form action="{{ route('user.comments.store') }}" method="POST">
                    @csrf
                    <div class="container my-4">
                        <strong>Add comments</strong>

                        <textarea name="title" class="shadow appearance-none border mt-2 rounded mb-4 w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" type="text">{!! $html ?? '' !!}
                        </textarea>
                    </div>

                    <input type="hidden" name="post_id" value="{{ $post->id }}">

                    <div class="container my-4">
                        <button type="submit" class="btn btn-blue">Comment</button>
                    </div>
                </form>
            </div>
            @endif
            <hr>

            <p class="my-4 mb-4">Comments ({{ $comments->count() }})</p>
            <hr>
            @foreach($comments as $comment)
                <div class="my-4 flex border">
                    <div>
                        <div class="p-2">
                            <div class="border shadow-md w-full">
                                <div class="flex">
                                    <a href="{{ route('user.profile.show', [$post->user]) }}">
                                    <img class="rounded-full" style="height: 50px; width: 50px;"  src="{{ asset('/'. $comment->user->avatar  ) }}" />
                                    </a>
                                </div>
                                <div>
                                    <h1 class="text-lg text-center">{{ $comment->user->name }}</h1>
                                    <p class="text-sm">{{ $comment->created_at->diffForHumans() }}</p>
                                </div>
                            </div>                          
                        </div>
                    </div>
                    <div class="my-4 w-4/5 border">
                        <p>{!! clean($comment->title) !!}</p>
                        @if(Auth::user())
                            @if($comment->commentOwner())
                            <div class="flex right-0 bottom-0">
                                <form action="{{ route('user.comments.destroy', $comment) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit">
                                    <img class="mr-3" style="height: 20px; width: 20px;" src="{{ asset('images/trashcan.png') }}">
                                </button>
                                </form>
                                <a href="{{ route('user.comments.edit', $comment) }}"><img style="height: 20px; width: 20px;" src="{{ asset('images/edit.png') }}"></a>
                            </div>
                            @endif
                            @endif
                    </div>
                </div>
            @endforeach
                
        </div>
</div>
@endsection
 

