@extends('layouts.app')

@section('head')
    <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
    <script>tinymce.init({ selector:'textarea' });</script>
@endsection

@section('content')

@if($errors->any())
    <div role="alert w-1/4">
    <div class="bg-red-500 w-1/4 text-white font-bold rounded-t px-4 py-2">
        There were problem with your input.
    </div>
    <div class="w-1/4 border border-t-0 border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    </div>
@endif
<div class="flex justify-center">
<div class="container p-2 lg:p-16 bg-white lg:w-2/3 w-11/12 rounded-lg">
    <div class="flex mb-8">
        <h1 class="text-black text-3xl font-bold uppercase mx-auto">create new Post</h1>
    </div>
    <form action="{{ route('user.posts.store') }}" method="POST" enctype="multipart/form-data" class="w-full">
                @csrf
                
        <div class="container my-4">
            <strong>Title</strong>
            <input type="text" name="title" class="shadow appearance-none border mt-2 rounded mb-4 w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" placeholder="Title">
        </div>
        

        <div class="container my-4">
            <strong>Description</strong>
            <input type="text" name="short_des" class="shadow appearance-none border mt-2 rounded mb-4 w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" placeholder="Short description of you're article.">
        </div>

        
        <div class="container my-4">
            <strong>Full Article</strong>
            <textarea name="body" class="shadow appearance-none border mt-2 rounded mb-4 w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" type="text">{!! $html ?? '' !!}</textarea>
        </div>
        

        
        <div class="container my-4">
            <strong class="block mb-2">Image:</strong>
            <label class="block text-sm text-gray-700 mb-2">• Acceptable image dimensions are minimum 900x300 and maximum 1250x500.</label>
            <label class="block text-sm text-gray-700">• File size cannot exceed 2 MB.</label> 
            
            <input type="file" name="cover" class="shadow appearance-none border mt-2 rounded mb-4 w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
        </div>
       

        <div class="container my-4">
            <strong>Category</strong>
            <select class="shadow appearance-none border mt-2 rounded mb-4 w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline bg-white" name="category_id">
                @foreach($categories as $category)
                    <option value="{{$category->id}}">
                        {{$category->name}}
                    </option>
                @endforeach
            </select>
	    </div>

        <div class="container my-4 flex justify-between">
            <a href="{{ route('user.posts.index') }}" class="shadow btn btn-blue text-xs md:text-lg">Cancel</a>
            <button type="submit" class="shadow btn btn-blue text-xs md:text-lg">Submit</button>
        </div>
    </form>
</div>
</div>
@endsection