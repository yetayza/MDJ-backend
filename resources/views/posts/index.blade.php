@extends('layouts.app')


@section('content')
@if($errors->any())
    <div class="bg-red-500">
        <button type="button" class="close" data-dismiss="alert"></button>	
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif    
@if($posts->count())

<div class="container bg-white mx-auto md:w-4/5 lg:w-3/4 xl:w-2/3 py-8">
    <div class="flex justify-end px-4 mb-4">
        <a href="{{ route('user.posts.create') }}" class="btn btn-blue">NEW POST</a>
    </div>

    <hr>
    @foreach($posts as $post)
    
    <div class="mt-8 mx-4 mb-4">
        <div class="h-24">
            <h3 class="h-8 text-xl font-bold mx-2 my-2">
                <a href="{{ route('user.posts.show', $post) }}">{{ Str::limit($post->title, 20) }}</a>
            </h3>
            <div>
                <h3 class="h-12 mx-2">
                    <a href="{{ route('user.posts.show', $post) }}">{{ Str::limit($post->short_des, 30) }}</a>
                </h3>
            </div>
        </div>
        
        <div class="h-10 mx-2 flex justify-start">
            <a href="{{ route('user.posts.edit', $post) }}" class="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 border border-blue-700 rounded">Edit</a>

            <form action="{{route('user.posts.destroy',$post)}}" method="POST">
                @csrf
                @method('DELETE')

            <button class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4  border-blue-700 rounded mx-2">Delete</button>
            </form>

        </div>
    </div>
    <hr class="my-1">
    @endforeach
</div>

@else
    <div class="container bg-white mx-auto p-4 md:w-2/3">
        <div class="flex justify-end px-4 mb-4">
            <a href="{{ route('user.posts.create') }}" class="btn btn-blue">NEW POST</a>
        </div>
        <p class="text-xl font-bold">You Currently dont have any post. <a href="{{ route('user.posts.create') }}">Click here to post new</a></p>
    </div>
@endif
@endsection
