@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card mb-4">
                <div class="card-header">Confirm Post delete.</div>

                <div class="card-body">
                    <p>Are you sure you'd like to delete <strong>{{ $post->title }}</strong>?</p>

                    <form action="{{ route('user.posts.destroy', $post) }}" method="POST">
                        @csrf
                        @method('DELETE')

                        <button type="submit" class="btn btn-danger">Yes, delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection