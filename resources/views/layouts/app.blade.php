<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-156906636-1"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-156906636-1');
		</script>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>{{ config('app.name', 'Laravel') }}</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:700italic,400,300,700' rel='stylesheet' type='text/css'>
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<!-- Scripts -->
		<script src="{{ asset('js/app.js') }}" defer></script>

		<!-- Fonts -->
		<link rel="dns-prefetch" href="//fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
		<link rel="stylesheet" href="{{ asset('css/app.css') }}"/>
		@yield('head')

	<style>
      #menu-toggle:checked + #menu {
		display: block;
	  }
		.dropdown:hover .dropdown-menu {
  			display: block;
		}
      
 	</style>
	</head>
	<body class="flex relative flex-col min-h-screen bg-gray-300">

	<!-- Header -->
		<header class="lg:px-16 px-6 bg-black flex flex-wrap items-center lg:py-0 py-2 mb-16 lg:mb-20 w-full">
			<div class="flex-1 flex justify-between items-center">
			   <div class="flex items-center flex-shrink-0 text-white mr-6">
			   		<a href="/" class="text-white">
			   			<img src="{{ asset('/images/laravellogo2.png'  ) }}" class="fill-current h-6 sm:h-10 w-8 sm:w-16 mr-2" width="54" height="54"/>
					</a>
					<a href="/" class="text-white">
						Laravel
					</a>
				</div>
				<form action="{{ route('search') }}" method="POST" role="search">
					@csrf
					<div class="relative mx-auto text-gray-600">
						<input class="w-32 text-xs sm:text-sm md:text-base sm:w-auto shadow mr-4 sm:mr-10 p-3 bg-white h-10 px-5 pr-10 border-2 border-gray-300 rounded-lg focus:outline-none"
						type="search" name="query" placeholder="Search" required>
						<button type="submit" class="absolute right-0 top-0 mt-3 mr-4 sm:mr-12">
						<svg class="text-gray-600 h-4 w-4 fill-current" xmlns="http://www.w3.org/2000/svg"
							xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px"
							viewBox="0 0 56.966 56.966" style="enable-background:new 0 0 56.966 56.966;" xml:space="preserve"
							width="512px" height="512px">
							<path
							d="M55.146,51.887L41.588,37.786c3.486-4.144,5.396-9.358,5.396-14.786c0-12.682-10.318-23-23-23s-23,10.318-23,23  s10.318,23,23,23c4.761,0,9.298-1.436,13.177-4.162l13.661,14.208c0.571,0.593,1.339,0.92,2.162,0.92  c0.779,0,1.518-0.297,2.079-0.837C56.255,54.982,56.293,53.08,55.146,51.887z M23.984,6c9.374,0,17,7.626,17,17s-7.626,17-17,17  s-17-7.626-17-17S14.61,6,23.984,6z" />
						</svg>
						</button>
					</div>
				</form>
			</div>

			<label for="menu-toggle" class="pointer-cursor lg:hidden block">
				<svg class="bg-gray-300 fill-current text-gray-900" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
					<title class="text-white">
						menu
					</title>
					<path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path>
				</svg>
			</label>
			<input class="hidden" type="checkbox" id="menu-toggle" />

			<div class="hidden lg:flex lg:items-center lg:w-auto w-full" id="menu">
				<nav class="w-full">
					<ul class="lg:flex items-center justify-between text-base text-white pt-4 lg:pt-0">
						<li>
							<a class="lg:p-4 py-3 px-0 text-gray-200 block border-b-4 border-transparent hover:border-white" href="/">
								Home
							</a>
						</li>
						@guest	
							<li class="{{ (request()->is('categories/index')) ? 'border-b-4 border-white' : '' }}">
								<a class="lg:p-4 py-3 px-0 block border-b-4 border-transparent hover:border-white" href="{{ route('categories.index') }}">
									Category
								</a>
							</li>
							<li>
								<a class="lg:p-4 py-3 px-0 block border-b-4 border-transparent hover:border-white" href="{{ route('login') }}">
									{{ __('Signin') }}
								</a>
							</li>
							@if (Route::has('register'))
								<li>
									<a class="lg:p-4 py-3 px-0 block border-b-4 border-transparent hover:border-white lg:mb-0 mb-2" href="{{ route('register') }}">
									{{ __('Signup') }}
									</a>
								</li>
							@endif
						@else
						@yield('nav-bar')
							<li class="{{ (request()->is('user/posts')) ? 'border-t-3 border-white' : '' }}">
            					<a href="{{ route('user.posts.index') }}" class="lg:p-4 py-3 px-0 block border-b-4 border-transparent hover:border-white">
									My Posts
								</a>
        					</li>
							
            				<li class="{{ (request()->is('user/categories')) ? 'border-t-3 border-white' : '' }}">
               					<a href="{{ route('user.categories.index') }}" class="lg:p-4 py-3 px-0 block border-b-4 border-transparent hover:border-white">
								   Categories
								</a>
            				</li>
							<li>
							<div class="">

								<div class="dropdown inline-block relative">
								<button class="bg-gray-300 text-gray-700 font-semibold py-2 px-4 rounded inline-flex items-center">
									<span class="mr-1 text-xs md:text-base">{{ Auth::user()->name }} </span>
								</button>
								<ul class="dropdown-menu absolute hidden text-gray-700 pt-1 h-3">
									
										<a class="bg-gray-200 text-xs md:text-base hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap" href="{{ route('user.profile.edit', $user) }}">
											User Profile
										</a>
										<a class="bg-gray-200 hover:bg-gray-400 text-xs md:text-base py-2 px-4 block whitespace-no-wrap" href="{{ route('logout') }}"
										onclick="event.preventDefault();
														document.getElementById('logout-form').submit();">
											{{ __('Logout') }}
										</a>
										<form id="logout-form" class="rounded-b bg-gray-200 hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap" action="{{ route('logout') }}" method="POST" style="display: none;">
											@csrf
										</form>
									
								</ul>
								</div>
							</div>
						@endguest
					</ul>
				</nav>
			</div>

		</header>
    
        <div class="flex-1 text-base relative bg-gray-300">

			<!-- Extra -->
			
				@include('posts.partials.flash-message')
				@yield('content')
			
		</div>
		<footer class="mt-16">
			<section class="bg-black py-8 w-full">
				<div class="container mx-auto px-8">
					<div class="table w-full">
						<div class="block sm:table-cell">
							<p class="uppercase text-white text-sm sm:mb-6">Links</p>
							<ul class="list-reset text-xs mb-6">
								<li class="mt-2 inline-block mr-2 sm:block sm:mr-0">
									<a href="#" class="text-white hover:text-white-dark">FAQ</a>
								</li>
								<li class="mt-2 inline-block mr-2 sm:block sm:mr-0">
									<a href="#" class="text-white hover:text-white-dark">Help</a>
								</li>
								<li class="mt-2 inline-block mr-2 sm:block sm:mr-0">
									<a href="#" class="text-white hover:text-white-dark">Support</a>
								</li>
							</ul>
						</div>
						<div class="block sm:table-cell">
							<p class="uppercase text-white text-sm sm:mb-6">Legal</p>
							<ul class="list-reset text-xs mb-6">
								<li class="mt-2 inline-block mr-2 sm:block sm:mr-0">
									<a href="#" class="text-white hover:text-white-dark">Terms</a>
								</li>
								<li class="mt-2 inline-block mr-2 sm:block sm:mr-0">
									<a href="#" class="text-white hover:text-white-dark">Privacy</a>
								</li>
							</ul>
						</div>
						<div class="block sm:table-cell">
							<p class="uppercase text-white text-sm sm:mb-6">Social</p>
							<ul class="list-reset text-xs mb-6">
								<li class="mt-2 inline-block mr-2 sm:block sm:mr-0">
									<a href="#" class="text-white hover:text-white-dark">Facebook</a>
								</li>
								<li class="mt-2 inline-block mr-2 sm:block sm:mr-0">
									<a href="#" class="text-white hover:text-white-dark">Linkedin</a>
								</li>
								<li class="mt-2 inline-block mr-2 sm:block sm:mr-0">
									<a href="#" class="text-white hover:text-white-dark">Twitter</a>
								</li>
							</ul>
						</div>
						<div class="block sm:table-cell">
							<p class="uppercase text-white text-sm sm:mb-6">Company</p>
							<ul class="list-reset text-xs mb-6">
								<li class="mt-2 inline-block mr-2 sm:block sm:mr-0">
									<a href="#" class="text-white hover:text-white-dark">Official Blog</a>
								</li>
								<li class="mt-2 inline-block mr-2 sm:block sm:mr-0">
									<a href="#" class="text-white hover:text-white-dark">About Us</a>
								</li>
								<li class="mt-2 inline-block mr-2 sm:block sm:mr-0">
									<a href="#" class="text-white hover:text-white-dark">Contact</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</section>
		</footer>

		@yield('script')
	</body>
		
</html>