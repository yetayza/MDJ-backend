<?php

namespace App;

use App\User;
use App\Comment;
use App\Category;
use Illuminate\Support\Str;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
use Illuminate\Database\Eloquent\Model;

class Post extends Model implements Searchable
{
    protected $guarded =[];

    public function comments()
    {
        return $this->hasMany(Comment::class, 'post_id');
    }
    /**
     *
     * 
     */
    public function setSlugAttribute($value) {

    if (static::whereSlug($slug = Str::slug($value))->exists()) {

        $slug = $this->incrementSlug($slug);
    }

        $this->attributes['slug'] = $slug;
    }

    public function incrementSlug($slug) {

    $original = $slug;

    $count = 2;

    while (static::whereSlug($slug)->exists()) {

        $slug = "{$original}-" . $count++;
    }

        return $slug;

    }

    /**
     *eloquent relation for user and post
     * 
     * @return [type] [description]
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     *eloquent releation for category and post
     * 
     * @return [type] [description]
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * use post url slug
     * 
     * @return [type] [description]
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * use in search result
     * 
     * @return [type] [description]
     */
    public function getSearchResult(): SearchResult
    {
        $url = route('posts.show', $this->slug);

        return new SearchResult(
            $this,
            $this->title,
            $url
        );
    }
}
