<?php

namespace App;

use App\Post;
use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class Category extends Model implements Searchable
{
    protected $guarded =[];

    /**
     * eloquent relation for category and post
     * 
     * @return [type] [description]
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    /**
     *use in search result
     * 
     * @return [type] [description]
     */
    public function getSearchResult(): SearchResult
    {
        $url = route('categories.show', $this->slug);

        return new SearchResult(
            $this,
            $this->name,
            $url
         );
    }
    /**
     * user category url slug
     * 
     * @return [type] [description]
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * user is category creator
     * 
     * @return [type] [description]
     */
    public function categoryOwner()
    {
        return $this->user_id == auth()->id();
    }
}

