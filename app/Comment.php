<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['title', 'post_id', 'user_id'];

    /**
     * 
     * [user description]
     * @return [type] [description]
     */
    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    /**
     * [post description]
     * @return [type] [description]
     */
    public function post()
    {
    	return $this->belongsTo(Post::class);
    }

    /**
     * commentOwner
     *
     * @return void
     */
    public function commentOwner()
    {
    	return $this->user_id == auth()->id();
    }
}
