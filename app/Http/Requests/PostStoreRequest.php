<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'short_des' => 'required',
            'body' => 'required',
            'cover' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:min_width=900,max_width=1250,min_height=300,max_height=500',
            'category_id' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'title.required' => 'Title is required!',
            'short_des.required' => 'Description is required!',
            'body.required' => 'body is required!',
            'cover.required' => 'Cover is required!',
            'cover.dimensions' => 'The image size is maximum 1250x500 and minimum 900x300',
            'category_id' => 'Category is required',
        ];
    }
}
