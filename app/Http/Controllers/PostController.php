<?php

namespace App\Http\Controllers;

use Auth;
use App\Post;
use Purifier;
use App\Category;
use App\Comment;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Requests\PostStoreRequest;
use App\Http\Requests\PostUpdateRequest;


class PostController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        // $posts = Post::where('user_id', '=', Auth::id())->paginate(20);
        $posts = auth()->user()->posts()->paginate(20);

        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();

        return view('posts.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostStoreRequest $request)
    {
        $imageName = time().'.'.$request->cover->extension();  
        $directory = 'images';

        $data = [
            'title' => request('title'),
            'short_des' => request('short_des'),
            'body' => Purifier::clean(request('body')),
            'user_id' => auth()->id(),
            'slug' => Str::slug($request->title, '-'),
            'category_id' => request('category_id'),
            'cover' => $directory. '/' . $imageName,
        ];
        $request->cover->move($directory, $imageName);

        $post = new Post($data);
        
        
        $post->save();


        return redirect()
                        ->route('user.posts.index')
                        ->with('success', 'A post created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        $comments = Comment::where('post_id', $post->id)->latest()->paginate(20);
        
        return view('posts.show', compact('post', 'comments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $categories = Category::all();

        if(!$this->checkAuth($post)){
            return redirect()
                    ->route('user.posts.index')
                    ->with('warning', 'Unauthorize to edit.');
        }

        return view('posts.edit', compact('post', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostUpdateRequest $request, Post $post)
    {
        if(!$this->checkAuth($post)){
            return redirect()
                    ->route('user.posts.index')
                    ->with('warning', 'Unauthorize to edit.');
        }

        $post->update($request->all());         

        if($request->hasFile('cover')) {
            $directory = 'images';
            $imageName = time().'.'.$request->cover->extension();
            $request->cover->move($directory, $imageName);
            $post->cover = $directory . '/' . $imageName;
        }

        $post->save();

        return redirect()
                        ->route('user.posts.index')
                        ->with('success', 'Post updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if(!$this->checkAuth($post)){
            return redirect()
                    ->route('user.posts.index')
                    ->with('error', 'Unauthorize to delete.');
        }
        $post->delete();
        return redirect()
                        ->route('user.posts.index')
                        ->with('warning', 'A post deleted.');
    }

    public function checkAuth($post)
    {
        return $post->user_id == Auth::id();
    }
}
