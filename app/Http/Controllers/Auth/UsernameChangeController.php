<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UsernameChangeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        if(!$this->authUser($user)){
        return redirect()
                ->route('user.posts.index')
                ->with('warning', 'Unauthorize action.');
    }
        return view('users.edit.username', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        if(!$this->authUser($user)){
            return redirect()
                    ->route('user.posts.index')
                    ->with('warning', 'Unauthorize action.');
        }
        $request->validate([          
            'name' => 'required'
        ]);
   
            $user->update($request->all());

        return redirect()
                        ->route('user.posts.index')
                        ->with('success', 'Username Changed successfully!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * authUser
     *
     * @param  mixed $user
     *
     * @return void
     */
    public function authUser($user)
    {
        return $user->id == Auth::id();
    }
    
}