<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use Illuminate\Http\Request;
Use App\Rules\MatchOldPassword;
use App\Http\Controllers\Controller;

class PictureChangeController extends Controller
{
	/**
	 * [index description]
	 * @param  User   $user [description]
	 * @return [type]       [description]
	 */
    public function index(User $user)
    {
    	if(!$this->authUser($user)) 
        {
            return back();
        }

    	return view('users.edit.imagechange', compact('user'));
    }


    /**
     * [update description]
     * @param  Request $Request [description]
     * @param  User    $user    [description]
     * @return [type]           [description]
     */
    public function update(Request $request, User $user)
    {	
    	if(!$this->authUser($user)) 
        {
            return back();
        }

        $request->validate([
        	'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

    	$user->update($request->all());
    	if($request->hasFile('avatar')) {
            $directory = 'images';
            $imageName = time().'.'.$request->avatar->extension();
            $request->avatar->move($directory, $imageName);
            $user->avatar = $directory . '/' . $imageName;
        }
        $user->save();
    	return redirect()
                        ->route('user.posts.index')
                        ->with('success', 'Username Changed successfully!!!');
    }

    public function authUser($user)
    {
        return $user->id == Auth::id();
    }
}
