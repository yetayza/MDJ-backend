<?php

namespace App\Http\Controllers;

use Auth;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\UserUpdateRequest;

class UsersController extends Controller
{
    public function profile(User $user)
    {
        if(!$this->authUser($user)){
            return redirect()
                    ->route('user.posts.index')
                    ->with('warning', 'Unauthorize action.');
        }

        return view('users.index', compact('user'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $posts = $user->posts()->latest()->paginate(20);
        return view('users.show', compact('user', 'posts'));
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        // $posts = Post::where('user_id', $user->id)->paginate(20);
        // return view('users.show', compact('user', 'posts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        if(!$this->authUser($user)){
            return redirect()
                    ->route('user.posts.index')
                    ->with('warning', 'Unauthorize action.');
        }
        return view('users.edit.imagechange', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, User $user)
    {
        if(!$this->authUser($user)){
            return redirect()
                    ->route('user.posts.index')
                    ->with('warning', 'Unauthorize action.');
        }

        $user->update($request->all());

        if($request->hasFile('avatar')) {
            $directory = 'images';
            $imageName = time().'.'.$request->avatar->extension();
            $request->avatar->move($directory, $imageName);
            $user->avatar = $directory . '/' . $imageName;
        }

        $user->save();

        return redirect()->route('users.show', $user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     *
     * 
     * @param  [type] $user [description]
     * @return [type]       [description]
     */
    public function authUser($user) 
    {
        return $user->id == Auth::id();
    }


}
