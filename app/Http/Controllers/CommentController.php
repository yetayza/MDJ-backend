<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CommentStoreRequest;
use App\Comment;
use App\Post;
use Auth;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentStoreRequest $request)
    {
        
        $data = [
            'title' => request('title'),
            'user_id' => auth()->id(),
            'post_id' => request('post_id'),
        ];

        $comment = new Comment($data);

        $comment->save();

        return back()
                    ->with('info', 'Commented on the post');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        if(!$this->checkAuth($comment)) {
            return back()
                        ->with('warning','Unauthorize to edit.');
        }

        $post = Post::where('id', $comment->post_id)->first();
        return view('comments.edit', compact('comment','post'));
    }

    /**
     * Update the specified resource in storage.
     * 
     * @param  Request $request [description]
     * @param  Comment $comment [description]
     * @param  Post    $post    [description]
     * @return [type]           [description]
     */
    public function update(Request $request, Comment $comment)
    {
        if(!$this->checkAuth($comment)) {
            return back()
                        ->with('warning','Unauthorize to edit.');
        }

        $comment->update($request->all());
        return redirect()
                        ->route('user.posts.show', $comment->post)
                        ->with('success', 'Comment Updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        if(!$this->checkAuth($comment)) {
            return back();
        }

        $comment->delete();
        return back()
                    ->with('warning', 'Comment Deleted.');
    }

    public function checkAuth($comment)
    {
        return $comment->user_id == auth()->id();
    }
}
