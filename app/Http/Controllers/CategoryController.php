<?php

namespace App\Http\Controllers;

use Auth;
use App\Post;
use App\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryStoreRequest;
use App\Http\Requests\CategoryUpdateRequest;

class CategoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::paginate(20);
        
        return view('categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::check()) {
            return back()
                    ->with('info', 'Please Log in first.');
        }
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryStoreRequest $request)
    {
        $data = [
            'name' => request('name'),
            'user_id' => auth()->id(),
            'slug' => $this->createSlug($request->name),
        ];
        
        $categories = new Category($data);

        $categories->save();
        return redirect()
                        ->route('user.categories.index')
                        ->with('success', 'Category created successfully!');
    }

   
    /**
     * createSlug
     *
     * @param  mixed $name
     * @param  mixed $id
     *
     * @return void
     */
    public function createSlug($name, $id = 0)
    {
       
        $slug = Str::slug($name, '-', '');
        
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        
        if (! $allSlugs->contains('slug', $slug)){
            return $slug;
        }
    
        for ($i = 1; $i <= 10; $i++) {
            $newSlug = $slug.'-'.$i;
            if (! $allSlugs->contains('slug', $newSlug)) {
                return $newSlug;
            }
        }
        throw new \Exception('Can not create a unique slug');
    }

    /**
     * getRelatedSlugs
     *
     * @param  mixed $slug
     * @param  mixed $id
     *
     * @return void
     */
    protected function getRelatedSlugs($slug, $id = 0)
    {
        return Category::select('slug')->where('slug', 'like', $slug.'%')
        ->where('id', '<>', $id)
        ->get();
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */

    public function show(Category $category)
     {
       $posts = $category->posts()->latest()->paginate(20);
       return view('categories.show', compact('posts'));
     }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        if(!$this->checkAuth($category)) {
            return redirect()
                    ->back()
                    ->with('info', 'Unauthorized to edit');
        }

        return view('categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryUpdateRequest $request, Category $category)
    {
        if(!$this->checkAuth($category)) {
            return redirect()
                    ->route('user.categories.index')
                    ->with('info', 'Unauthorized to edit');
        }

        $category->slug = Str::slug($request->name, '-');
        $category->update($request->all());
        return redirect()
                        ->route('user.categories.index')
                        ->with('success', 'Category updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {   
        if(!$this->checkAuth($category)) {
            return back()
                    ->with('danger', 'Unauthorized to delete');
        }

        $category->delete();
        return redirect()
                        ->route('user.categories.index')
                        ->with('info', 'Category Deleted.');
    }


    /**
     * checkAuth
     *
     * @param  mixed $category
     *
     * @return void
     */
    public function checkAuth($category)
    {
        return $category->user_id == auth()->id();
    }
}
