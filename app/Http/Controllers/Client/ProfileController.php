<?php


namespace App\Http\Controllers\Client;

use Auth;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserUpdateRequest;

class ProfileController extends Controller
{
    public function index(User $user)
    {
        $posts = $user->posts()->latest()->paginate(20);

        return view('client.users.show', compact('user', 'posts'));
    }
}
