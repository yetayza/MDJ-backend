<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class ViewController extends Controller
{
    /**
     * homepage for all
     * 
     * @return [type] [description]
     */
    public function welcome()
    {
        $posts = Post::latest()->paginate(20); 
        return view('welcome', compact('posts'));
    }
}
