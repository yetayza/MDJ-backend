<?php

Auth::routes(['verify' => 'true']);

Route::get('/', 'ViewController@welcome')->name('view.welcome');

Route::resource('posts', 'Client\PostController')->only(['index', 'show']);

Route::resource('categories', 'Client\CategoryController')->only(['index', 'show']);

Route::get('profile/{user}', 'Client\ProfileController@index')->name('profile.show');

Route::post('/search', 'HomeController@search')->name('search');

Route::get('/home', 'HomeController@index');

Route::prefix('user')->name('user.')->group(function() {
	Route::group(['middleware' => 'verified', 'auth'], function(){
		Route::get('{user}/profile/changepicture', 'Auth\PictureChangeController@index')->name('change.picture.edit');

		Route::post('{user}/profile/changepicture', 'Auth\PictureChangeController@update')->name('change.picture.update');	

		Route::get('{user}/profile/changepassword', 'Auth\PasswordChangeController@index')->name('change.password.edit');

		Route::post('{user}/profile/changepassword', 'Auth\PasswordChangeController@store')->name('change.password.update');
		
		Route::get('{user}/profile/changeusername', 'Auth\UsernameChangeController@edit')->name('change.username.edit');
		
		Route::patch('{user}/profile/changeusername', 'Auth\UsernameChangeController@update')->name('change.username.update');

		Route::resource('/', 'UsersController');
		
		Route::get('profile/{user}/edit', 'UsersController@profile')->name('profile.edit');
		Route::resource('posts', 'PostController')->except(['index', 'show']);
		Route::resource('categories', 'CategoryController')->except(['index', 'show']);
		Route::resource('comments', 'CommentController');
	});
});

Route::prefix('user')->name('user.')->group(function() {
	Route::group(['middleware' => 'auth'], function() {
		Route::get('profile/{user}', 'UsersController@index')->name('profile.show');
		
		Route::resource('posts', 'PostController')->only(['index', 'show']);
		Route::resource('categories', 'CategoryController')->only(['index', 'show']);
	});
});

